<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use JWTAuth;

class ProductController extends Controller
{
    protected $user;

    public function __construct(){

        $this->user = JWTAuth::parseToken()->authenticate();
    }

    public function index(){

	    $products =  $this->user->products()->get(['name', 'price', 'details']);

	    if(!count($products)) {
	        return response()->json([
	            'success' => false,
	            'message' => 'Sorry, product not found for this user.'
	        ], 400);
	    }

        return response()->json(["status" => true,"products" => $products
        ]);
    }

    public function show($id){
	    $product = $this->user->products()->find($id);
	 
	    if (!$product) {
	        return response()->json([
	            'success' => false,
	            'message' => 'Sorry, product with id ' . $id . ' cannot be found'
	        ], 400);
	    }
	 
	    return $product;
    }

    public function store(Request $request){

    	$this->validate($request, [
            "name" => "required|string",
            "price" => "required|integer|min:1",
            "details" => "required|string"
        ]);

        $product = new Product();
	    $product->name = $request->name;
	    $product->price = $request->price;
	    $product->details = $request->details;
	 
	    if ($this->user->products()->save($product))
	        return response()->json([
	            'success' => true,
	            'product' => $product
	        ]);
	    else

        return response()->json([
            'success' => false,
            'message' => 'Sorry, product could not be added'
        ], 500);
    }
}
